import React from 'react'

export default class Schedule extends React.Component {
  static propTypes = {
    items: React.PropTypes.array.isRequired,
    add: React.PropTypes.func.isRequired,
    change: React.PropTypes.func.isRequired
  }

  componentDidMount() {
    this.props.add(1, 15, 30, 'Lorem')
    this.props.add(2, 10, 0, 'Foo')
  }

  drag = (e) => {
    e.dataTransfer.setData('text', e.target.id)
  }

  drop = (e) => {
    e.preventDefault()

    var data = e.dataTransfer.getData('text')

    var id = parseInt(data.split('-').pop())

    var timeString = e.target.parentElement.children[0].innerText
    var parts = timeString.split(':')

    // Push update to store
    this.props.change(id, parts[0], parts[1])

    // Don't use this, it starts a shitshow
    // e.target.appendChild(document.getElementById(data))
  }

  dragOver = (e) => {
    e.preventDefault()
  }

  render() {
    var rows = []

    for (var h = 9; h <= 17; h ++) {
      rows[h] = []

      for (var m = 0; m < 60; m += 15) {
        rows[h][m] = (
          <tr>
            <td>{h}:{m}</td>
            <td onDrop={this.drop} onDragOver={this.dragOver}></td>
          </tr>
        )
      }
    }

    this.props.items.forEach((item) => {
      rows[item.hour][item.minute] = (
        <tr>
          <td>{item.hour}:{item.minute}</td>
          <td onDrop={this.drop} onDragOver={this.dragOver}>
            <div id={"item-" + item.id} draggable="true" onDragStart={this.drag}>{item.description}</div>
          </td>
        </tr>
      )
    })

    return (
      <div style={{ margin: '0 auto' }} >
        <table>
          <tbody>
            {rows}
          </tbody>
        </table>
      </div>
    )
  }
}
